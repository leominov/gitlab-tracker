module github.com/leominov/gitlab-tracker

go 1.14

require (
	github.com/cloudfoundry/cli v6.36.2+incompatible
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.3
	github.com/hashicorp/hcl v1.0.1-0.20191016231534-914dc3f8dd7c
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.10.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	github.com/xanzy/go-gitlab v0.19.0
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/yaml.v2 v2.2.4
)
